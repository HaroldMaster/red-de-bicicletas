var Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};
Bicicleta.prototype.toString = function () {
  return "id: " + this.id + "| color: " + this.color;
};
Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
  Bicicleta.allBicis.push(aBici);
};
Bicicleta.buscar = function (aid) {
  var bici = Bicicleta.allBicis.find((x) => x.id == aid);
  if (bici) {
    return bici;
  } else {
    throw new Error("No existe la bicicleta");
  }
};
Bicicleta.eliminar = function (id) {
  for (let i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == id) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
};

var b1 = new Bicicleta(1, "negra", "abc", [-0.225219, -78.5248]);
var b2 = new Bicicleta(2, "roja", "def", [-0.22522, -78.523]);
Bicicleta.add(b1);
Bicicleta.add(b2);

module.exports = Bicicleta;
