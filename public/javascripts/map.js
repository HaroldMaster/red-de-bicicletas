var mymap = L.map('mapid').setView([-0.225219, -78.5248],15);
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);
//var marker = L.marker([-0.225219, -78.5248]).addTo(mymap);
$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(element => {
            L.marker([element.ubicacion[0],element.ubicacion[1]],{title:element.id}).addTo(mymap);
        });
    }
});