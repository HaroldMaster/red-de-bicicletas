var express = require("express");
var router = express.Router();
var bicicletaController = require("../controllers/bicicleta");
router.get("/", bicicletaController.bicicleta_list);
router.get("/formulario", bicicletaController.bicicleta_create_get);
router.post("/formulario", bicicletaController.bicicleta_create_post);
router.get("/:id/editar", bicicletaController.bicicleta_update_get);
router.post("/editar", bicicletaController.bicicleta_update_post);
router.post("/:id/delete", bicicletaController.bicicleta_delete_post);

module.exports = router;
